const path = require('path')
const dbHost = process.env.DB_HOST || 'localhost'
const dbPort = process.env.DB_PORT || 27017
const dbName = process.env.DB_NAME || 'my_db_name'
const mongoUrl = `mongodb://${dbHost}:${dbPort}/${dbName}`
console.log(dbHost)
console.log(dbName)
console.log(mongoUrl)